package com.devcamp.s10.jbr4_10;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr410Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr410Application.class, args);
	}

}
