package com.devcamp.s10.jbr4_10;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class controller {
    @CrossOrigin
    @GetMapping("/circle-area")
    public double getCircleArea(){
        Circle HinhTron1 = new Circle(13, "green");
        Circle HinhTron2 = new Circle(24);
        System.out.println(HinhTron1);
        System.out.println(HinhTron2);
        System.out.println("Dien tich hinh tron 1 la: " + HinhTron1.getArea());
        System.out.println("Dien tich hinh tron 2 la: " + HinhTron2.getArea());

        return HinhTron1.getArea();
    }

    @CrossOrigin
    @GetMapping("/cylinder-volume")
    public double getCylinderVolume(){
        Cylinder HinhTru1 = new Cylinder(14, "black", 50);
        Cylinder HinhTru2 = new Cylinder(31, 41);
        System.out.println(HinhTru1);
        System.out.println(HinhTru2);
        System.out.println("the tich hinh tru 1 la: " + HinhTru1.getVolume());
        System.out.println("the tich hinh tru 2 la: " + HinhTru2.getVolume());
        return HinhTru2.getVolume();
    }
}
